import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:myapp/components/add_item.dart';
import 'package:myapp/components/requested_items.dart';
import 'firebase_options.dart';
import './components/login_page.dart';
import './components/signup_page.dart';
import './components/home_page.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Use a StreamBuilder to conditionally set the home page based on authentication status
      home: StreamBuilder<User?>(
        stream: FirebaseAuth.instance.authStateChanges(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            // Return a loading indicator while checking authentication status
            return const Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            );
          } else if (snapshot.hasData) {
            return const HomePage(isWeb: kIsWeb);
          } else {
            // If user is not authenticated, redirect to the login page
            return const LoginPage();
          }
        },
      ),
      routes: {
        '/login': (context) => const LoginPage(),
        '/signup': (context) => const SignupPage(),
        '/home': (context) => const HomePage(isWeb: kIsWeb),
        '/requests': (context) => RequestedItemsPage(),
        '/item': (context) => AddItemPage(
              isWeb: kIsWeb,
            ),
      },
    );
  }
}


/**
import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class CatFactModel {
  final String type;
  final String text;
  final String createdAt;
  final String updatedAt;

  CatFactModel({
    required this.text,
    required this.type,
    required this.createdAt,
    required this.updatedAt,
  });

  factory CatFactModel.fromJson(Map<String, dynamic> json) {
    return CatFactModel(
      text: json['text'],
      type: json['type'],
      createdAt: json['createdAt'],
      updatedAt: json['updatedAt'],
    );
  }
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const CounterPage(),
    );
  }
}

class CounterPage extends StatefulWidget {
  const CounterPage({Key? key}) : super(key: key);

  @override
  _CounterPageState createState() => _CounterPageState();
}

class _CounterPageState extends State<CounterPage> {
  int _counter = 0;
  int _incrementCount = 0;
  int _decrementCount = 0;
  Color _backgroundColor = Colors.white;

  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _idController = TextEditingController();
  String? username;
  int? id;
  File? _image;
  String? _imagePath;
  Uint8List? _imageForWeb;
  late Future<Position> _position;
  GoogleMapController? _controller;
  late Future<List<CatFactModel>> _catFacts;

  @override
  void initState() {
    super.initState();
    _loadDataFromFirebase();
    _catFacts = fetchCatFacts();
    _position = _determinePosition();
  }

  Future<List<CatFactModel>> fetchCatFacts() async {
    final response = await http.get(
      Uri.parse(
          'https://cat-fact.herokuapp.com/facts/random?animal_type=cat&amount=2'),
    );
    if (response.statusCode == 200) {
      List<dynamic> data = jsonDecode(response.body);
      List<CatFactModel> catFacts = data
          .map((fact) => CatFactModel.fromJson(fact as Map<String, dynamic>))
          .toList();
      return catFacts;
    } else {
      throw Exception('Failed to load cat facts');
    }
  }

  Future<void> _loadDataFromPrefs() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      username = prefs.getString('username') ?? '';
      id = prefs.getInt('id') ?? 0;
      _usernameController.text = username ?? '';
      _idController.text = id?.toString() ?? '';
    });
  }

  Future<void> _saveDataToPrefs() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('username', _usernameController.text);
    prefs.setInt('id', int.parse(_idController.text));
  }

  Future<void> _loadDataFromFirebase() async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    DocumentSnapshot ds =
        await firestore.collection('student').doc('tRShe1uxhTssP65mSp5q').get();
    if (ds.data() != null) {
      Map<String, dynamic> data = (ds.data() as Map<String, dynamic>);
      setState(() {
        username = data['username'] ?? '';
        id = data['id'] ?? 0;
        _usernameController.text = username ?? '';
        _idController.text = id?.toString() ?? '';
      });
    }
  }

  Future<void> _saveDataToFirebase() async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    firestore.collection('student').doc('tRShe1uxhTssP65mSp5q').set({
      'username': _usernameController.text,
      'id': int.parse(_idController.text),
    }).then((value) {
      if (kDebugMode) {
        print('count updated successfully');
      }
    }).catchError((error) {
      if (kDebugMode) {
        print('write error: $error');
      }
    });
  }

  void _incrementCounter() {
    setState(() {
      _counter++;
      _incrementCount++;
      _backgroundColor = Colors.green;
    });
  }

  void _decrementCounter() {
    setState(() {
      _counter--;
      _decrementCount++;
      _backgroundColor = Colors.red;
    });
  }

  void _resetCounter() {
    setState(() {
      _counter = 0;
      _backgroundColor = Colors.white;
    });
  }

  Future<void> _getImage() async {
    final picker = ImagePicker();
    final XFile? pickedImage =
        await picker.pickImage(source: ImageSource.camera);
    _imageForWeb = await pickedImage!.readAsBytes();
    if (kIsWeb) {
      setState(() {
        _imagePath = pickedImage.path;
      });
    } else {
      setState(() {
        _image = File(pickedImage.path);
      });
    }
  }

  Future<void> _upload() async {
    if (_image != null || _imagePath != null) {
      // Upload image file to storage (using uid) and generate a downloadURL
      final String downloadURL = await _uploadFile("1");
      await _addItem(downloadURL, "Old_Image");
    } else {
      if (kDebugMode) {
        print('No image to upload');
      }
    }
  }

  Future<String> _uploadFile(String filename) async {
    if (kIsWeb) {
      final storageRef = FirebaseStorage.instance.ref();
      try {
        // upload the raw photo data
        TaskSnapshot uploadTask =
            await storageRef.child('$filename.jpg').putData(
                _imageForWeb!,
                SettableMetadata(
                  contentType: 'image/jpeg',
                  contentLanguage: 'en',
                ));
        final String downloadURL = await uploadTask.ref.getDownloadURL();
        return downloadURL;
      } on FirebaseException catch (e) {
        return '_uploadFile on web error: $e';
      }
    } else {
      // Create a reference to file location in Google Cloud Storage object
      Reference ref = FirebaseStorage.instance.ref().child('$filename.jpg');
      // Add metadata to the image file
      final metadata = SettableMetadata(
        contentType: 'image/jpeg',
        contentLanguage: 'en',
      );
      // Upload the file to Storage
      final UploadTask uploadTask = ref.putFile(_image!, metadata);
      TaskSnapshot uploadResult = await uploadTask;
      // After the upload task is complete, get a (String) download URL
      final String downloadURL = await uploadResult.ref.getDownloadURL();
      // Return the download URL (to be used in the database entry)
      return downloadURL;
    }
  }

  Future<void> _addItem(String downloadURL, String title) async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    firestore.collection('photo').doc('31Kqrt8fvRHS7QHIl0Tg').set({
      'downloadURL': downloadURL,
      'title': title,
      'timestamp': DateTime.now(),
    }).then((value) {
      if (kDebugMode) {
        print('count updated successfully');
      }
    }).catchError((error) {
      if (kDebugMode) {
        print('write error: $error');
      }
    });
  }

  /// Determine the current position of the device.
  ///
  /// When the location services are not enabled or permissions
  /// are denied the `Future` will return an error.
  /// Ref:- https://pub.dev/packages/geolocator
  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return await Geolocator.getCurrentPosition();
  }

  void _onMapCreated(GoogleMapController controller) {
    setState(() {
      _controller = controller;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Counter App'),
      ),
      backgroundColor: _backgroundColor,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    FutureBuilder<Position>(
                      future: _position,
                      builder: (context, snapshot) {
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return const CircularProgressIndicator();
                        } else if (snapshot.hasError) {
                          return Text('Error: ${snapshot.error}');
                        } else {
                          return Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                'Latitude: ${snapshot.data?.latitude}',
                                style: const TextStyle(fontSize: 18),
                              ),
                              Text(
                                'Longitude: ${snapshot.data?.longitude}',
                                style: const TextStyle(fontSize: 18),
                              ),
                            ],
                          );
                        }
                      },
                    ),
                    SingleChildScrollView(
                        scrollDirection:
                            Axis.horizontal, // Specify horizontal scrolling
                        child: Row(
                            mainAxisSize: MainAxisSize
                                .min, // Optional: constrain row width
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Text(
                                    "Curent Pic",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18,
                                    ),
                                  ),
                                  Text(
                                    '${DateTime.now().toString()}  ',
                                    style: const TextStyle(
                                      fontStyle: FontStyle.italic,
                                      fontSize: 14,
                                    ),
                                  ),
                                  const SizedBox(height: 10),
                                  _image == null
                                      ? const SizedBox.shrink()
                                      : Image.file(
                                          _image!,
                                          height: 100,
                                          width: 100,
                                        ),
                                  _imagePath == null
                                      ? const SizedBox.shrink()
                                      : Image.network(
                                          _imagePath!,
                                          height: 100,
                                          width: 100,
                                        ),
                                ],
                              ),
                              StreamBuilder(
                                stream: FirebaseFirestore.instance
                                    .collection('photo')
                                    .doc('31Kqrt8fvRHS7QHIl0Tg')
                                    .snapshots(),
                                builder: (context,
                                    AsyncSnapshot<DocumentSnapshot> snapshot) {
                                  if (snapshot.connectionState ==
                                      ConnectionState.waiting) {
                                    return const CircularProgressIndicator();
                                  } else if (snapshot.hasError) {
                                    return Text('Error: ${snapshot.error}');
                                  } else {
                                    if (snapshot.data!.exists) {
                                      var data = snapshot.data!.data()
                                          as Map<String, dynamic>;
                                      var title = data['title'];
                                      var timestamp = data['timestamp'] != null
                                          ? DateTime.fromMillisecondsSinceEpoch(
                                              data['timestamp'].seconds * 1000)
                                          : null;
                                      var downloadURL = data['downloadURL'];
                                      return Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            title,
                                            style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18,
                                            ),
                                          ),
                                          if (timestamp != null)
                                            Text(
                                              timestamp.toString(),
                                              style: const TextStyle(
                                                fontStyle: FontStyle.italic,
                                                fontSize: 14,
                                              ),
                                            ),
                                          const SizedBox(height: 10),
                                          Image.network(
                                            downloadURL,
                                            height: 100,
                                            width: 100,
                                          ),
                                        ],
                                      );
                                    } else {
                                      return const Text(
                                          'Document does not exist');
                                    }
                                  }
                                },
                              )
                            ])),
                    FutureBuilder<List<CatFactModel>>(
                      future: _catFacts,
                      builder: (context, snapshot) {
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return const CircularProgressIndicator();
                        } else if (snapshot.hasError) {
                          return Text('Error: ${snapshot.error}');
                        } else {
                          List<CatFactModel>? facts = snapshot.data;
                          return SizedBox(
                            height: 200, // Set a fixed finite height
                            child: ListView.builder(
                              itemCount: facts!.length,
                              itemBuilder: (context, index) {
                                CatFactModel fact = facts[index];
                                return Card(
                                  child: ListTile(
                                    title: Text('Type: ${fact.type}'),
                                    subtitle: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text('Text: ${fact.text}'),
                                        Text('Created At: ${fact.createdAt}'),
                                        Text('Updated At: ${fact.updatedAt}'),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            ),
                          );
                        }
                      },
                    ),
                    TextField(
                      controller: _usernameController,
                      onChanged: (value) {
                        setState(() {
                          username = value;
                        });
                        _saveDataToFirebase();
                      },
                      decoration: const InputDecoration(labelText: 'Username'),
                    ),
                    TextField(
                      controller: _idController,
                      onChanged: (value) {
                        setState(() {
                          id = int.tryParse(value);
                        });
                        _saveDataToFirebase();
                      },
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.allow(RegExp(r'^[0-9]+$')),
                      ],
                      keyboardType: TextInputType.number,
                      decoration: const InputDecoration(labelText: 'Id'),
                    ),
                    SingleChildScrollView(
                      scrollDirection:
                          Axis.horizontal, // Specify horizontal scrolling
                      child: Row(
                        mainAxisSize:
                            MainAxisSize.min, // Optional: constrain row width
                        children: [
                          Text(
                            'Counter: $_counter',
                            style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w700,
                              color: Colors.black,
                            ),
                          ),
                          const SizedBox(width: 10),
                          Text(
                            'Increment Count: $_incrementCount',
                            style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w400,
                              color: Color(0xff0984f6),
                            ),
                          ),
                          const SizedBox(width: 10),
                          Text(
                            'Decrement Count: $_decrementCount',
                            style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w400,
                              color: Color(0xff09e6f6),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal, // Specify horizontal scrolling
              child: Row(
                mainAxisSize: MainAxisSize.min, // Optional: constrain row width
                children: [
                  ElevatedButton(
                    onPressed: () {
                      setState(() {
                        _catFacts = fetchCatFacts();
                      });
                    },
                    child: const Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text('Get Cats Data'),
                        SizedBox(width: 8), // Add spacing between text and icon
                        Icon(Icons.refresh),
                      ],
                    ),
                  ),
                  const SizedBox(width: 10),
                  ElevatedButton(
                    onPressed: _upload,
                    style: ElevatedButton.styleFrom(
                      backgroundColor: const Color.fromARGB(255, 41, 60, 77),
                    ),
                    child: const Text(
                      'Upload Image',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  const SizedBox(width: 10),
                  ElevatedButton(
                    onPressed: _incrementCounter,
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.blue,
                    ),
                    child: const Text(
                      'Increment Counter',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  const SizedBox(width: 10),
                  ElevatedButton(
                    onPressed: _decrementCounter,
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.orange,
                    ),
                    child: const Text(
                      'Decrement Counter',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  const SizedBox(width: 10),
                  ElevatedButton(
                    onPressed: _resetCounter,
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.grey,
                    ),
                    child: const Text(
                      'Reset Counter',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _getImage,
        tooltip: kIsWeb ? 'Upload Image' : 'Take a photo',
        child: const Icon(Icons.camera),
      ),
    );
  }
}
*/