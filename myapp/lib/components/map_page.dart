import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class MapPage extends StatefulWidget {
  final String msgId;
  final String chatNum;

  const MapPage({Key? key, required this.msgId, required this.chatNum})
      : super(key: key);

  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  GoogleMapController? _controller;
  Future<Position>? _currentPosition;

  @override
  void initState() {
    super.initState();
    _currentPosition = _getCurrentLocation();
  }

  void _onMapCreated(GoogleMapController controller) {
    setState(() {
      _controller = controller;
    });
  }

  Future<Position> _getCurrentLocation() async {
    var ds = await FirebaseFirestore.instance
        .collection(widget.msgId)
        .doc('1')
        .collection('chats')
        .doc(widget.chatNum)
        .get();
    if (ds.data() != null) {
      Map<String, dynamic> data = (ds.data() as Map<String, dynamic>);
      return Position(
        latitude: data['lat'],
        longitude: data['long'],
        timestamp: DateTime.now(),
        accuracy: 0.0,
        heading: 0.0,
        speed: 0.0,
        speedAccuracy: 0.0,
        altitude: 0.0,
        altitudeAccuracy: 0.0,
        headingAccuracy: 0.0,
      );
    }

    return await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Map'),
      ),
      body: FutureBuilder<Position>(
        future: _currentPosition,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return Text('Error: ${snapshot.error}');
          } else {
            final position = snapshot.data!;
            return GoogleMap(
              onMapCreated: _onMapCreated,
              initialCameraPosition: CameraPosition(
                target: LatLng(
                  position.latitude,
                  position.longitude,
                ),
                zoom: 15.0,
              ),
              markers: {
                Marker(
                  markerId: const MarkerId('currentLocation'),
                  position: LatLng(
                    position.latitude,
                    position.longitude,
                  ),
                  infoWindow: const InfoWindow(title: 'Current Location'),
                ),
              },
            );
          }
        },
      ),
    );
  }
}
