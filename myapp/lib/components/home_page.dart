import 'package:flutter/material.dart';
import 'package:myapp/components/search_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './app_drawer.dart';
import './item_list.dart';

class HomePage extends StatefulWidget {
  final bool isWeb;

  const HomePage({Key? key, required this.isWeb}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

 @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bool isWeb = widget.isWeb;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
        actions: [
            IconButton(
              icon: const Icon(Icons.search),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        SearchPage(),
                  ),
                );
              },
            ),
          ],
      ),
      drawer: const AppDrawer(),
      body: Center(
        child: ItemList(isWeb: isWeb),
      ),
    );
  }
}