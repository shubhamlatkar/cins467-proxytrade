import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class NotificationPage extends StatefulWidget {
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  List<String> itemIds = [];
  List<String> chatIds = [];
  List<DocumentSnapshot> messages = [];

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  Future<void> fetchData() async {
    try {
      // Get item IDs
      final itemQuery = await FirebaseFirestore.instance
          .collection('users')
          .doc(FirebaseAuth.instance.currentUser?.uid)
          .collection('items')
          .get();

      
        print(itemQuery.docs.map((doc) => doc.id).toList());
      

      /*
      // Get chat IDs for each item
      for (String itemId in itemIds) {
        final existingChatDoc = await FirebaseFirestore.instance
            .collection('items')
            .doc(itemId)
            .collection('chats')
            .get();
        final existingChatId = existingChatDoc.docs[0].data()['id'];

        setState(() {
          chatIds.add(existingChatId);
        });
      }

      // Listen for chat messages
      for (String chatId in chatIds) {
        FirebaseFirestore.instance
            .collection('messages')
            .doc(chatId)
            .collection('chats')
            .orderBy('timestamp', descending: true)
            .snapshots()
            .listen((snapshot) {
          setState(() {
            messages = snapshot.docs;
          });
        });
      }
      */
    } catch (error) {
      print('Error fetching data: $error');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Notifications'),
      ),
      body: const Text("Hi"),
    );
  }
}