import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import "./display_item.dart";

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  String searchText = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TextField(
          decoration: const InputDecoration(
            hintText: 'Search Items',
            icon: Icon(Icons.search),
          ),
          onChanged: (text) => setState(() => searchText = text),
        ),
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: searchText.isEmpty
            ? FirebaseFirestore.instance
                .collection('items')
                .limit(10)
                .snapshots()
            : FirebaseFirestore.instance
                .collection('items')
                .where('title', isGreaterThanOrEqualTo: searchText.toLowerCase())
                .where('title', isLessThanOrEqualTo: '${searchText.toLowerCase()}\uf8ff')
                .limit(10)
                .snapshots(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          }
          final items = snapshot.data?.docs ?? [];
          return ListView.builder(
            itemCount: items.length,
            itemBuilder: (context, index) {
              final Map<String, dynamic>? itemMap = items[index].data() as Map<String, dynamic>?;
              final List<dynamic>? images = (itemMap ?? {})['images'] as List<dynamic>?; // Add null check and cast
              final String firstImage = images?.isNotEmpty == true ? images![0] : ''; // Add null check
              String title = ((itemMap ?? {})['title'] as String?) ?? '';
              String price = ((itemMap ?? {})['price'] as String?) ?? '';
              return Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: InkWell(
                  onTap: () {
                    Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => DisplayItem(itemId: items[index].id),
                    ),
                  );
                  },
                  child: Card(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Image.network(
                          firstImage,
                          width: double.infinity,
                          height: 200,
                          fit: BoxFit.cover,
                        ),
                        // Item name (replace with actual item name)
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            title,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        // Item price (replace with actual item price)
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
                          child: Text(
                            '\$${price}', // Item price
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          );
        },
      ),
    );
  }
}
