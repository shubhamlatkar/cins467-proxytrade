import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:geolocator/geolocator.dart';
import 'map_page.dart';

class ChatPage extends StatefulWidget {
  final String userId;
  final String chatId;

  const ChatPage({super.key, required this.userId, required this.chatId});

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  final TextEditingController _textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Chat'),
        actions: [
          IconButton(
            icon: const Icon(Icons.location_on),
            onPressed: () {
              _sendMessage(widget.userId, 'location');
            },
          ),
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: StreamBuilder<QuerySnapshot>(
              stream: FirebaseFirestore.instance
                  .collection('messages')
                  .doc(widget.chatId)
                  .collection('chats')
                  .orderBy('timestamp', descending: true)
                  .snapshots(),
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  return Center(
                    child: Text('Error: ${snapshot.error}'),
                  );
                } else if (snapshot.connectionState ==
                    ConnectionState.waiting) {
                  return const Center(child: CircularProgressIndicator());
                } else {
                  final messages = snapshot.data!.docs;
                  return ListView.builder(
                    reverse: true,
                    itemCount: messages.length,
                    itemBuilder: (context, index) {
                      final message = messages[index];
                      final String uid = message['uid'];
                      final bool isCurrentUser = uid == widget.userId;
                      final String messageId = message.id;
                      final bool isLocationMessage =
                          message['type'] == 'location';

                      return ListTile(
                        title: isLocationMessage
                            ? ElevatedButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => MapPage(
                                                msgId: widget.chatId,
                                                chatNum: messageId,
                                              )));
                                },
                                child: Text(
                                  "Location",
                                  textAlign: isCurrentUser
                                      ? TextAlign.right
                                      : TextAlign.left,
                                ),
                              )
                            : Text(
                                message['text'],
                                textAlign: isCurrentUser
                                    ? TextAlign.right
                                    : TextAlign.left,
                              ),
                        subtitle: Text(
                          message['timestamp'].toDate().toString(),
                          textAlign:
                              isCurrentUser ? TextAlign.right : TextAlign.left,
                        ),
                      );
                    },
                  );
                }
              },
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: _textEditingController,
                    decoration: const InputDecoration(
                      hintText: 'Type a message',
                    ),
                  ),
                ),
                IconButton(
                  icon: const Icon(Icons.send),
                  onPressed: () => _sendMessage(widget.userId, 'msg'),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _sendMessage(String uid, String type) async {
    final text = _textEditingController.text.trim();
    if (text.isNotEmpty || type == 'location') {
      try {
        var messageData = {
          'timestamp': FieldValue.serverTimestamp(),
          'uid': uid,
          'type': type,
          'text': ''
        };

        if (type == 'msg') {
          messageData['text'] = text;
        } else if (type == 'location') {
          // Get current position
          Position position = await Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.high,
          );
          messageData['lat'] = position.latitude;
          messageData['long'] = position.longitude;
        }

        await FirebaseFirestore.instance
            .collection('messages')
            .doc(widget.chatId)
            .collection('chats')
            .add(messageData);

        _textEditingController.clear();
      } catch (e) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('Error'),
              content: const Text('Failed to send message. Please try again.'),
              actions: [
                TextButton(
                  onPressed: () => Navigator.pop(context),
                  child: const Text('OK'),
                ),
              ],
            );
          },
        );
      }
    }
  }
}
