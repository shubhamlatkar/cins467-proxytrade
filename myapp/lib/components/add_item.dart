import 'dart:async';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AddItemPage extends StatefulWidget {
  final bool isWeb;

  const AddItemPage({super.key, required this.isWeb});

  @override
  _AddItemPageState createState() => _AddItemPageState();
}

class _AddItemPageState extends State<AddItemPage> {
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final TextEditingController _priceController = TextEditingController();

  List<File> _selectedImages = [];
  List<String> _imagePath = [];
  final List<Uint8List> _imageForWeb = [];
  List<String> _urls = [];

  Future<void> _getImage() async {
    final picker = ImagePicker();
    final XFile? pickedImage =
        await picker.pickImage(source: ImageSource.camera);
    _imageForWeb.add(await pickedImage!.readAsBytes());
    if (widget.isWeb) {
      setState(() {
        _imagePath.add(pickedImage.path);
      });
    } else {
      setState(() {
        _selectedImages.add(File(pickedImage.path));
      });
    }
  }

  void _deleteImage(int index) {
    setState(() {
      if (widget.isWeb) {
        _imageForWeb.removeAt(index);
        _imagePath.removeAt(index);
      } else {
        _selectedImages.removeAt(index);
      }
    });
  }

  Future<void> _uploadImages() async {
    if (widget.isWeb) {
      final storageRef = FirebaseStorage.instance.ref();

      try {
        for (var i = 0; i < _imagePath.length; ++i) {
          var name = '${DateTime.now().toString()}-$i.jpg';
          name = name.replaceAll(' ', '-');
          TaskSnapshot uploadTask = await storageRef.child(name).putData(
              _imageForWeb[i],
              SettableMetadata(
                contentType: 'image/jpeg',
                contentLanguage: 'en',
              ));
          final String downloadURL = await uploadTask.ref.getDownloadURL();
          _urls.add(downloadURL);
        }
      } on FirebaseException catch (e) {
        _urls.add('_uploadFile on web error: $e');
      }
    } else {
      for (var i = 0; i < _selectedImages.length; ++i) {
        var name = '${DateTime.now().toString()}-$i.jpg';
        name = name.replaceAll(' ', '-');
        Reference ref = FirebaseStorage.instance.ref().child(name);
        final metadata = SettableMetadata(
          contentType: 'image/jpeg',
          contentLanguage: 'en',
        );
        final UploadTask uploadTask = ref.putFile(_selectedImages[i], metadata);
        TaskSnapshot uploadResult = await uploadTask;
        final String downloadURL = await uploadResult.ref.getDownloadURL();
        _urls.add(downloadURL);
      }
    }
  }

  Future<void> _addItemToFirestore() async {
    // Add item details to Firestore
    var itemData = {
      'title': _titleController.text,
      'description': _descriptionController.text,
      'price': _priceController.text,
      'images': _urls,
      'sold': "false"
    };

    var doc = await FirebaseFirestore.instance
        .collection('users')
        .doc(FirebaseAuth.instance.currentUser?.uid)
        .collection('items')
        .add(itemData);
    await FirebaseFirestore.instance
        .collection('items')
        .doc(doc.id)
        .set(itemData);

  }

  Future<void> _submitItem() async {
    // Upload images to Firebase Storage
    await _uploadImages();

    // Add item details to Firestore
    await _addItemToFirestore();

    // Clear form fields and selected images
    _titleController.clear();
    _descriptionController.clear();
    _priceController.clear();
    setState(() {
      _selectedImages = [];
      _imagePath = [];
      _urls = [];
    });

    // Show success message or navigate to a different page
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(content: Text('Item added successfully!')),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add Item'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextFormField(
                controller: _titleController,
                decoration: const InputDecoration(labelText: 'Title'),
              ),
              TextFormField(
                controller: _descriptionController,
                decoration: const InputDecoration(labelText: 'Description'),
              ),
              TextFormField(
                controller: _priceController,
                decoration: const InputDecoration(labelText: 'Price'),
                keyboardType: TextInputType.number,
              ),
              const SizedBox(height: 20.0),
              const Text('Images:'),
              const SizedBox(height: 10.0),
              SizedBox(
                height: 150,
                child: ListView.builder(
                    itemCount: widget.isWeb
                        ? _imagePath.length
                        : _selectedImages.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return SizedBox(
                        width: 150,
                        height: 150,
                        child: Column(
                          children: [
                            widget.isWeb
                                ? Image.network(_imagePath[index],
                                    width: 100, height: 100)
                                : Image.file(_selectedImages[index],
                                    width: 100, height: 100),
                            ElevatedButton(
                                onPressed: () {
                                  _deleteImage(index);
                                },
                                child: const Text('Delete'))
                          ],
                        ),
                      );
                    }),
              ),
              const SizedBox(width: 10.0),
              ElevatedButton(
                onPressed: _getImage,
                child: const Text('Add Image'),
              ),
              const SizedBox(height: 20.0),
              ElevatedButton(
                onPressed: _submitItem,
                child: const Text('Submit'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
