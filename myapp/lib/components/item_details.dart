import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:myapp/components/chat_page.dart';
import 'package:myapp/components/edit_item.dart';

class ItemDetails extends StatelessWidget {
  final String itemId;
  final bool isWeb;
  const ItemDetails({Key? key, required this.isWeb, required this.itemId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Item Options'),
          actions: [
            IconButton(
              icon: const Icon(Icons.edit),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        EditItem(isWeb: isWeb, itemId: itemId),
                  ),
                );
              },
            ),
          ],
        ),
        body: StreamBuilder<QuerySnapshot>(
          stream: FirebaseFirestore.instance
              .collection('items')
              .doc(itemId)
              .collection('chats')
              .snapshots(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(child: CircularProgressIndicator());
            }
            final chatDocs = snapshot.data?.docs ?? [];
            if (chatDocs.isEmpty) {
              return const Center(child: Text('No chats available'));
            }
            return ListView.builder(
              itemCount: chatDocs.length,
              itemBuilder: (context, index) {
                final chatData = chatDocs[index].data() as Map<String, dynamic>;
                return ElevatedButton(
                  onPressed: () {
                    // Navigate to ChatPage with item id
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) =>
                            ChatPage(userId: FirebaseAuth.instance.currentUser?.uid ?? "", chatId: chatData['id']),
                      ),
                    );
                  },
                  child: Text('Chat with ${chatData['name']}'),
                );
              },
            );
          },
        ));
  }
}
