import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './chat_page.dart';

class DisplayItem extends StatelessWidget {
  final String itemId;

  const DisplayItem({Key? key, required this.itemId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Item Details'),
      ),
      body: StreamBuilder<DocumentSnapshot>(
        stream: FirebaseFirestore.instance
            .collection('items')
            .doc(itemId)
            .snapshots(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          }
          if (snapshot.hasError) {
            return Text('Error: ${snapshot.error}');
          }

          final itemData = snapshot.data?.data() as Map<String, dynamic>?;
          if (itemData == null) {
            return const Text('Item not found');
          }

          final String title = itemData['title'] ?? '';
          final String description = itemData['description'] ?? '';
          final String price = itemData['price'] ?? '';
          final List<dynamic>? images = itemData['images'] as List<dynamic>?;

          return SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // Display item title
                  Text(
                    title,
                    style: const TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: 10.0),
                  // Display item description
                  Text(description),
                  const SizedBox(height: 10.0),
                  // Display item price
                  Text(
                    '\$$price',
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 20.0),

                  ElevatedButton(
                    onPressed: () async {
                      final currentUserId =
                          FirebaseAuth.instance.currentUser?.uid ?? "";
                      // Check if a chat already exists
                      var existingChatDoc = await FirebaseFirestore.instance
                          .collection('items')
                          .doc(itemId)
                          .collection('chats')
                          .where('userId', isEqualTo: currentUserId)
                          .get();
                      if (existingChatDoc.docs.isNotEmpty) {
                        // Existing chat found, use its ID
                        final existingChatId =
                            existingChatDoc.docs[0].data()['id'];
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ChatPage(
                              userId: currentUserId,
                              chatId: existingChatId, // Use existing chat ID
                            ),
                          ),
                        );
                      } else {
                        DocumentSnapshot<Map<String, dynamic>> snapshot =
                            await FirebaseFirestore.instance
                                .collection('users')
                                .doc(currentUserId)
                                .get();
                        Set<String> ids = {};
                        if (snapshot.exists) {
                          final itemData = snapshot.data();
                          if (itemData != null && itemData['itemId'] != null) {
                            ids = (itemData['itemId'] as List<dynamic>)
                                .cast<String>()
                                .toSet();
                          }
                        }
                        ids.add(itemId);
                        await FirebaseFirestore.instance
                            .collection('users')
                            .doc(currentUserId)
                            .set({"itemId": ids});

                        // No existing chat, create a new message and chat document
                        var messageDoc = await FirebaseFirestore.instance
                            .collection('messages')
                            .add({});

                        await FirebaseFirestore.instance
                            .collection('items')
                            .doc(itemId)
                            .collection('chats')
                            .add({
                          "id": messageDoc.id,
                          "name":
                              FirebaseAuth.instance.currentUser?.displayName,
                          "userId": currentUserId,
                        });

                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ChatPage(
                              userId: currentUserId,
                              chatId:
                                  messageDoc.id, // Use newly created chat ID
                            ),
                          ),
                        );
                      }
                    },
                    child: const Text('Chat'),
                  ),
                  const SizedBox(height: 20.0),
                  SizedBox(
                    height: 150,
                    child: ListView.builder(
                        itemCount: images!.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          return SizedBox(
                            width: 150,
                            height: 150,
                            child: Column(
                              children: [
                                Image.network(images[index],
                                    width: 100, height: 100),
                              ],
                            ),
                          );
                        }),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
