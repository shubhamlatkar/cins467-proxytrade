import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AppDrawer extends StatefulWidget {
  const AppDrawer({Key? key});

  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          const DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
            child: Text(
              'Menu',
              style: TextStyle(
                color: Colors.white,
                fontSize: 24,
              ),
            ),
          ),
          // Conditionally display login or logout based on authentication status
          StreamBuilder<User?>(
            stream: FirebaseAuth.instance.authStateChanges(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const CircularProgressIndicator();
              } else if (snapshot.hasData) {
                // User is logged in, display logout option
                return ListTile(
                  title: const Text('Logout'),
                  onTap: () async {
                    await FirebaseAuth.instance.signOut();
                    Navigator.pushNamed(context, '/login');
                  },
                );
              } else {
                // User is not logged in, display login and signup options
                return Column(
                  children: [
                    ListTile(
                      title: const Text('Login'),
                      onTap: () {
                        Navigator.pushNamed(context, '/login');
                      },
                    ),
                    ListTile(
                      title: const Text('Sign Up'),
                      onTap: () {
                        Navigator.pushNamed(context, '/signup');
                      },
                    ),
                  ],
                );
              }
            },
          ),
          ListTile(
            title: const Text('Add Item'),
            onTap: () {
              Navigator.pushNamed(context, '/item');
            },
          ),
          ListTile(
            title: const Text('Requested Items'),
            onTap: () async {
              Navigator.pushNamed(context, '/requests');
            },
          ),
        ],
      ),
    );
  }
}
