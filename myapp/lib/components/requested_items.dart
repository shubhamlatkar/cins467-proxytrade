import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import './display_item.dart';

class RequestedItemsPage extends StatefulWidget {
  @override
  _RequestedItemsPageState createState() => _RequestedItemsPageState();
}

class _RequestedItemsPageState extends State<RequestedItemsPage> {
  final currentUserId = FirebaseAuth.instance.currentUser?.uid ?? "";

  Future<List<DocumentSnapshot>> _fetchRequestedItems() async {
    // Fetch requested item IDs from user document
    final userSnapshot = await FirebaseFirestore.instance
        .collection('users')
        .doc(currentUserId)
        .get();

    if (!userSnapshot.exists) {
      return []; // Handle case where user document doesn't exist
    }

    final itemData = userSnapshot.data();
    if (itemData != null && itemData['itemId'] != null) {
      final requestedItemIds = (itemData['itemId'] as List<dynamic>)
          .cast<String>()
          .toList();

      // Fetch item details based on requested IDs
      final itemSnapshots = await FirebaseFirestore.instance
          .collection('items')
          .where(FieldPath.documentId, whereIn: requestedItemIds)
          .get();
      return itemSnapshots.docs;
    } else {
      return []; // Handle case where user has no 'itemId' field
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Requested Items'),
      ),
      body: FutureBuilder<List<DocumentSnapshot>>(
        future: _fetchRequestedItems(), // Fetch data on widget build
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          }
          if (snapshot.hasError) {
            return Text('Error: ${snapshot.error}');
          }

          final items = snapshot.data ?? []; // Handle empty list

          return ListView.builder(
            itemCount: items.length,
            itemBuilder: (context, index) {
              final item = items[index];
              final Map<String, dynamic>? itemMap = item.data() as Map<String, dynamic>?;
              final List<dynamic>? images = (itemMap ?? {})['images'] as List<dynamic>?;
              final String firstImage = images?.isNotEmpty == true ? images![0] : '';
              String title = ((itemMap ?? {})['title'] as String?) ?? '';
              String price = ((itemMap ?? {})['price'] as String?) ?? '';
              return Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => DisplayItem(itemId: item.id),
                      ),
                    );
                  },
                  child: Card(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Image.network(
                          firstImage,
                          width: double.infinity,
                          height: 200,
                          fit: BoxFit.cover,
                        ),
                        // Item name (replace with actual item name)
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            title,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        // Item price (replace with actual item price)
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
                          child: Text(
                            '\$$price', // Item price
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          );
        },
      ),
    );
  }
}
