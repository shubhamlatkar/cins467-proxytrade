import 'dart:async';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';

class EditItem extends StatefulWidget {
  final bool isWeb;
  final String itemId;

  const EditItem({Key? key, required this.isWeb, required this.itemId})
      : super(key: key);

  @override
  _EditItemState createState() => _EditItemState();
}

class _EditItemState extends State<EditItem> {
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final TextEditingController _priceController = TextEditingController();
  bool isSold = false;

  List<File> _selectedImages = [];
  List<String> _imagePath = [];
  final List<Uint8List> _imageForWeb = [];
  List<String> _urls = [];

  @override
  void initState() {
    super.initState();
    // Fetch item details from Firestore
    _fetchItemDetails();
  }

  Future<void> _fetchItemDetails() async {
    try {
      final DocumentSnapshot itemSnapshot = await FirebaseFirestore.instance
          .collection('users')
          .doc(FirebaseAuth.instance.currentUser?.uid)
          .collection('items')
          .doc(widget.itemId)
          .get();

      if (itemSnapshot.exists) {
        final Map<String, dynamic>? itemData =
            itemSnapshot.data() as Map<String, dynamic>?;

        _titleController.text = itemData?['title'] ?? '';
        _descriptionController.text = itemData?['description'] ?? '';
        _priceController.text = itemData?['price'] ?? '';
        final List<dynamic>? images = itemData?['images'] as List<dynamic>?;

        setState(() {
          _urls = List<String>.from(images ?? []);
        });
      }
    } catch (e) {
      print('Error fetching item details: $e');
    }
  }

  Future<void> _updateItemInFirestore() async {
    // Update item details in Firestore
    var itemData = {
      'title': _titleController.text,
      'description': _descriptionController.text,
      'price': _priceController.text,
      'images': _urls,
      'sold': isSold
    };
    await FirebaseFirestore.instance
        .collection('users')
        .doc(FirebaseAuth.instance.currentUser?.uid)
        .collection('items')
        .doc(widget.itemId)
        .update(itemData);
    if (isSold) {
      await FirebaseFirestore.instance
          .collection('items')
          .doc(widget.itemId)
          .delete();
    } else {
      await FirebaseFirestore.instance
          .collection('items')
          .doc(widget.itemId)
          .set(itemData);
    }
  }

  Future<void> _submitItem() async {
    // Upload images to Firebase Storage...
    await _uploadImages();

    // Update item details in Firestore
    await _updateItemInFirestore();

    // Clear form fields and selected images...
    _titleController.clear();
    _descriptionController.clear();
    _priceController.clear();
    setState(() {
      _selectedImages = [];
      _imagePath = [];
      _urls = [];
    });

    // Show success message or navigate to a different page...
    Navigator.pushNamed(context, '/home');
  }

  Future<void> _getImage() async {
    final picker = ImagePicker();
    final XFile? pickedImage =
        await picker.pickImage(source: ImageSource.camera);
    _imageForWeb.add(await pickedImage!.readAsBytes());
    if (widget.isWeb) {
      setState(() {
        _imagePath.add(pickedImage.path);
      });
    } else {
      setState(() {
        _selectedImages.add(File(pickedImage.path));
      });
    }
  }

  void _deleteImage(int index) {
    setState(() {
      if (widget.isWeb) {
        _imageForWeb.removeAt(index);
        _imagePath.removeAt(index);
      } else {
        _selectedImages.removeAt(index);
      }
    });
  }

  void _deleteImageSaved(int index) async {
    try {
      await FirebaseStorage.instance.refFromURL(_urls[index]).delete();
    } catch (e) {
      print(e);
    }
    setState(() {
      _urls.removeAt(index);
    });
  }

  Future<void> _uploadImages() async {
    if (widget.isWeb) {
      final storageRef = FirebaseStorage.instance.ref();

      try {
        for (var i = 0; i < _imagePath.length; ++i) {
          var name = '${DateTime.now().toString()}-$i.jpg';
          name = name.replaceAll(' ', '-');
          TaskSnapshot uploadTask = await storageRef.child(name).putData(
              _imageForWeb[i],
              SettableMetadata(
                contentType: 'image/jpeg',
                contentLanguage: 'en',
              ));
          final String downloadURL = await uploadTask.ref.getDownloadURL();
          _urls.add(downloadURL);
        }
      } on FirebaseException catch (e) {
        _urls.add('_uploadFile on web error: $e');
      }
    } else {
      for (var i = 0; i < _selectedImages.length; ++i) {
        var name = '${DateTime.now().toString()}-$i.jpg';
        name = name.replaceAll(' ', '-');
        Reference ref = FirebaseStorage.instance.ref().child(name);
        final metadata = SettableMetadata(
          contentType: 'image/jpeg',
          contentLanguage: 'en',
        );
        final UploadTask uploadTask = ref.putFile(_selectedImages[i], metadata);
        TaskSnapshot uploadResult = await uploadTask;
        final String downloadURL = await uploadResult.ref.getDownloadURL();
        _urls.add(downloadURL);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Edit Item'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextFormField(
                controller: _titleController,
                decoration: const InputDecoration(labelText: 'Title'),
              ),
              TextFormField(
                controller: _descriptionController,
                decoration: const InputDecoration(labelText: 'Description'),
              ),
              TextFormField(
                controller: _priceController,
                decoration: const InputDecoration(labelText: 'Price'),
                keyboardType: TextInputType.number,
              ),
              const SizedBox(height: 20.0),
              const Text('Images:'),
              const SizedBox(height: 10.0),
              SizedBox(
                height: 150,
                child: ListView.builder(
                    itemCount: _urls.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return SizedBox(
                        width: 150,
                        height: 150,
                        child: Column(
                          children: [
                            Image.network(_urls[index],
                                width: 100, height: 100),
                            ElevatedButton(
                                onPressed: () {
                                  _deleteImageSaved(index);
                                },
                                child: const Text('Delete'))
                          ],
                        ),
                      );
                    }),
              ),
              SizedBox(
                height: 150,
                child: ListView.builder(
                    itemCount: widget.isWeb
                        ? _imagePath.length
                        : _selectedImages.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return SizedBox(
                        width: 150,
                        height: 150,
                        child: Column(
                          children: [
                            widget.isWeb
                                ? Image.network(_imagePath[index],
                                    width: 100, height: 100)
                                : Image.file(_selectedImages[index],
                                    width: 100, height: 100),
                            ElevatedButton(
                                onPressed: () {
                                  _deleteImage(index);
                                },
                                child: const Text('Delete'))
                          ],
                        ),
                      );
                    }),
              ),
              const SizedBox(width: 10.0),
              ElevatedButton(
                onPressed: _getImage,
                child: const Text('Add Image'),
              ),
              const SizedBox(height: 20.0),
              const Text('Mark Sold'),
              ToggleButtons(
                isSelected: [
                  !isSold,
                  isSold
                ], // Initial selection based on isSold
                onPressed: (selectedIndex) =>
                    setState(() => isSold = selectedIndex == 1),
                children: const [
                  Icon(Icons.cancel), // Un-sold icon
                  Icon(Icons.done), // Sold icon
                ],
              ),
              const SizedBox(height: 20.0),
              ElevatedButton(
                onPressed: _submitItem,
                child: const Text('Submit'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
